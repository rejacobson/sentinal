require(['jquery', 'lib/input', 'game', 'app/level', 'app/arena', 'app/emitterlist', 'app/player', 'app/nexus', 'app/dialog'],
function($, Input, Game, Level, Arena, EmitterList, Player, Nexus, Dialog) {

console.log('---- Main');
console.log(Level);

  var player = new Player();

  Input.on('keydown.w', function(){ player.moving.up = true; })
       .on('keydown.s', function(){ player.moving.down = true; })
       .on('keydown.a', function(){ player.moving.left = true; })
       .on('keydown.d', function(){ player.moving.right = true; });

  Input.on('keyup.w', function(){ player.moving.up = false; })
       .on('keyup.s', function(){ player.moving.down = false; })
       .on('keyup.a', function(){ player.moving.left = false; })
       .on('keyup.d', function(){ player.moving.right = false; });

  Input.on('keydown.up',    function(){ player.shooting.up = true;    })
       .on('keydown.down',  function(){ player.shooting.down = true;  })
       .on('keydown.left',  function(){ player.shooting.left = true;  })
       .on('keydown.right', function(){ player.shooting.right = true; });

  Input.on('keyup.up',    function(){ player.shooting.up = false;    })
       .on('keyup.down',  function(){ player.shooting.down = false;  })
       .on('keyup.left',  function(){ player.shooting.left = false;  })
       .on('keyup.right', function(){ player.shooting.right = false; });

  var gamepads = {};

  function gamepadHandler(event, connecting) {
    var gamepad = event.gamepad;
    // Note:
    // gamepad === navigator.getGamepads()[gamepad.index]

    console.log('connected '+ gamepad.index);

    if (connecting) {
      gamepads[gamepad.index] = gamepad;
    } else {
      delete gamepads[gamepad.index];
    }
  }

  window.addEventListener("gamepadconnected", function(e) { gamepadHandler(e, true); }, false);
  window.addEventListener("gamepaddisconnected", function(e) { gamepadHandler(e, false); }, false);

  function poll_gamepads() {
    if (gamepads[0]) {
      var g = gamepads[0];

      var moveX = g.axes[0];
      var moveY = g.axes[1];
      var shootX = g.axes[2];
      var shootY = g.axes[3];

      player.moving.up = false;
      player.moving.down = false;
      player.moving.left = false;
      player.moving.right = false;

      player.shooting.up = false;
      player.shooting.down = false;
      player.shooting.left = false;
      player.shooting.right = false;

      // right
      if (g.axes[0] > 0) {
        player.moving.right = true;
      }

      // left
      if (g.axes[0] < 0) {
        player.moving.left = true;
      }

      // down
      if (g.axes[1] > 0) {
        player.moving.down = true;
      }

      // up
      if (g.axes[1] < 0) {
        player.moving.up = true;
      }

      // shoot right
      if (g.axes[2] > 0) {
        player.shooting.right = true;
      }

      // shoot left
      if (g.axes[2] < 0) {
        player.shooting.left = true;
      }

      // shoot down
      if (g.axes[3] > 0) {
        player.shooting.down = true;
      }

      // shoot up
      if (g.axes[3] < 0) {
        player.shooting.up = true;
      }
    }
  }

  //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
  $(function() {

    console.log('Initialized the project with jQuery');

    var con = $('#console');

    var ticks = 0, fps = 0;
    var update_loop = function(dt) {
      ticks++;
      fps += dt;

      if (ticks >= 100) {
        fps = 1 / (fps / 100);
        con.html(fps.toPrecision(3) +' fps');

        ticks = 0;
        fps = 0;
      }

      poll_gamepads();
      Arena.update(dt);
      EmitterList.update(dt);
    }

    var game = new Game(update_loop);

    Arena.append(player).insert(player);
    player.setPosition(200, 200);

    Arena.append(Nexus).insert(Nexus);
    Nexus.placeAtCenter();

    game.start();

    Dialog.open('instructions');

    $(document).on('game.start', function() {
      Dialog.close();
      player.setPosition(Nexus.position[0] - 60, Nexus.position[1]).activate();
      Nexus.restore();
      Level.load(1);
    })
    .on('game.lasttoken', function() {
      Level.next();
    })
    .on('game.retry', function() {
      Dialog.close();
      player.setPosition(Nexus.position[0] - 60, Nexus.position[1]).activate();
      Nexus.restore();
      Level.load(1);
    })
    .on('game.instructions', function() {
      Dialog.open('instructions');
    })
    .on('game.win', function() {
      Dialog.open('win');
    })
    .on('game.wave', function(event, num) {
      Dialog.create('Wave '+ num);
    })
    .on('game.gameover', function() {
      player.retire();
      Dialog.open('gameover');
    });

  });

});
