define(['jquery', 'underscore', 'lib/input'], function($, _, Input) {

  var requestAnimationFrame = (function(){
    //Check for each browser
    //@paul_irish function
    //Globalises this function to work on any browser as each browser has a different namespace for this
    try{
      window;
    } catch(e){
      return;
    }
    
    return window.requestAnimationFrame || //Chromium
           window.webkitRequestAnimationFrame || //Webkit
           window.mozRequestAnimationFrame || //Mozilla Geko
           window.oRequestAnimationFrame || //Opera Presto
           window.msRequestAnimationFrame || //IE Trident?
           function(callback, element) { //Fallback function
             window.setTimeout(callback, 1000/60);
           }
       
  })();

  var pageHiddenProperty = (function(){
    var prefixes = ['webkit','moz','ms','o'];
    
    // if 'hidden' is natively supported just return it
    if ('hidden' in document) return 'hidden';
    
    // otherwise loop over all the known prefixes until we find one
    for (var i = 0; i < prefixes.length; i++){
      if ((prefixes[i] + 'Hidden') in document) return prefixes[i] + 'Hidden';
    }

    // otherwise it's not supported
    return null;
  })();

  function pageIsHidden() {
    if (!pageHiddenProperty) return false;
    return document[pageHiddenProperty];
  };

  function onVisibilityChange(hidden_callback, visible_callback) {
    if (pageHiddenProperty) {
      var eventname = pageHiddenProperty.replace(/[H|h]idden/, '') + 'visibilitychange';
      document.addEventListener(eventname, function(event){
        if (pageIsHidden()) {
          if (hidden_callback && _.isFunction(hidden_callback)) hidden_callback(event);
        } else {
          if (visible_callback && _.isFunction(visible_callback)) visible_callback(event);
        }
      });
    }
  };

  var Game = function(update_callback) {

    var paused = false,
        dt, last_t = 0;

    // Game loop
    // Update entities
    this.start = function() {
      last_t = 0;
      tick(16);
    } 

    this.pause = function() {
      paused = true;
    }

    this.unpause = function() {
      var t = (new Date()).getTime();
      paused = false;
      last_t =  t - 16;
      tick(t);
    }

    this.fps = function() {
      return frames_per_sec;
    }

    function tick(t) {
      t = t || (new Date()).getTime();
      dt = (t - last_t) * 0.001; // number of seconds since last update
      last_t = t;

      if (update_callback) update_callback(dt);

      if (!paused) requestAnimationFrame(tick);
    }

    var self = this;
    onVisibilityChange(function(){
      self.pause(); 
    }, function(){
      self.unpause();
    });
  };
 
  return Game;

});
