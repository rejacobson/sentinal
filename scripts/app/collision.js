define(['underscore'], function(_) {

  var CollisionStats = (function() {
    var tests = {};

    return {
      add: function(e1, e2) {
        var keys = this.keys(e1, e2);
        tests[keys[0]] = true;
        tests[keys[1]] = true;
      },

      tested: function(e1, e2) {
        var keys = this.keys(e1, e2);
        return _.has(tests, keys[0]) || _.has(tests, keys[1]);
      },

      keys: function(e1, e2) {
        return [
          e1.id +':'+ e2.id,
          e2.id +':'+ e1.id
        ];
      },

      tests: function() {
        return tests;
      },

      reset: function() {
        tests = {};
      }
    };

  })();


  var CollisionResolver = function(EntityList) {
    var e1, e2;

    CollisionStats.reset();

    _.each(EntityList._population(), function(count, index) {
      index = parseInt(index);

      var current_cell = EntityList._cell(index), 
          surrounding_cells = EntityList._surroundingCells(index),
          current_size = current_cell.length,
          combined = current_cell.concat(surrounding_cells);

      // Loop through each entity in current cell
      for (var i=0; i<current_size; ++i) {
        e1 = combined[i];

        if (!e1.isInteracting()) continue;

        // Test against every other entity
        for (var j=i+1, jlen=combined.length; j<jlen; ++j) {
          e2 = combined[j];

          if (!e2.isInteracting() || CollisionStats.tested(e1, e2)) continue;

          CollisionStats.add(e1, e2); 

          // Test e1 and e2
          if (e1.intersects(e2)) {
            if (e1.collision) e1.collision(e2);
            if (e2.collision) e2.collision(e1);
          }
        };
      };

    });

  };

  return CollisionResolver;

});
