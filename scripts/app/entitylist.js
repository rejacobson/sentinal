define(['underscore', 'lib/spatialpartition', 'app/collision'], function(_, GridPartition, CollisionResolver) {
  
  var EntityList = function(options) {
    if (!_.isArray(options.mapsize) || options.mapsize.length != 2) throw new Error('EntityList map_size must be a 2 element array: [width, height]');
    if (!_.isArray(options.cellsize) || options.cellsize.length != 2) throw new Error('EntityList cells must be a 2 element array: [cell_width, cell_height]');

    GridPartition.call(this, options.mapsize, options.cellsize, options.offset);

    var _entities = [],
        _size = 0; 

    _.extend(this, {

      // Return the number of active entities in the system
      size: function() {
        return _size;
      },

      get: function() {
        return _.compact(_entities);
      },

      // Insert a new entity
      insert: function(entity) {
        if (entity._cell_index != undefined) return;

        var cell_index = this._indexByPosition(entity.position);
       
        _entities.push(entity);

        _size++;

        this.reindex(cell_index, entity);
      },

      // Remove an entity
      remove: function(entity) {
        if (entity._cell_index) this._remove(entity._cell_index, entity);

        var i = _.indexOf(_entities, entity);
        if (-1 != i) {
          _entities[i] = undefined;
        }

        _size--;

        delete entity._cell_index;
      },

      // Reposition the entity within the spatial partition
      reindex: function(index, entity) {
        if (index == entity._cell_index) return;
        
        if (entity._cell_index) {
          this._remove(entity._cell_index, entity);
        }

        entity._cell_index = index;
        this._insert(index, entity);
      },

      // Execute a function for each entity in the system
      eachEntity: function(func) {
        for (var i=0, len = _entities.length; i<len; ++i) {
          if (!_entities[i] || _entities[i].isRetired()) continue;
          if (false === func.call(this, _entities[i], i)) break;
        }
      },

      // Update all entities
      update: function(dt) {
        var e, new_index, dead = [];

        this.eachEntity(function(e) {
          if (e.update) e.update(dt);

          new_index = this._indexByPosition(e.position);
          if (null === new_index) {
            dead.push(e);
          } else if (e._cell_index != new_index) {
            this.reindex(new_index, e);
          }
        });

        if (dead.length) {
          for (var i=0, len=dead.length; i<len; ++i) {
            this.remove(dead[i]);
            if (dead[i].retire) dead[i].retire();
          }
        }

        CollisionResolver(this); 
      }
    });

  };

  EntityList.prototype = Object.create(GridPartition.prototype);

  return EntityList;

});
