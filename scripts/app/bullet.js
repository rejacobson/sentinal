define(['jquery', 'underscore', 'lib/inheritance', 'app/entity', 'app/nexus'], function($, _, Class, Entity, Nexus) {

  var Bullet = Class.extend(Entity, {
    initialize: function() {
      this.parent('bullet');

      this.html().addClass('bullet animate rotate forever one-second'); //.data('bullet', this);

      this.max_life = 2000;
      this.life = this.max_life;
    },

    activate: function() {
      this.life = this.max_life;
      this.parent();
      return this;
    },

    collision: function(entity) {
      if (entity.name == 'enemy') {
        this.retire();
        Nexus.powerup();
      }
    },

    update: function(dt) {
      this.life -= (dt * 1000);
      if (this.life <= 0) {
        this.retire();
        return;
      }
      this.parent(dt);
    }
  });

  $(document).ready(function() {

    Bullet.size = (function(){
      var b = new Bullet();
      var el = b.html().appendTo('body').hide();
      var size = [el.width(), el.height()];
      el.remove();
      return size;
    })();

  });


  return Bullet;

});
