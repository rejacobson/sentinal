define(['jquery', 'app/entitylist'], function($, EntityList) {

  var Arena = (function(){
    var $html = null;
    var width, height;
    var Entities = new EntityList({
      mapsize: [850, 730],
      cellsize: [50, 50],
      offset: [50, 50]
    });
    var player;
  
    return {
      html: function() {
        if (!$html) $html = $('#arena');
        return $html;
      },

      width: function() {
        if (!width) width = this.html().width();
        return width;  
      },

      height: function() {
        if (!height) height = this.html().height();
        return height;  
      },

      center: function() {
        return [this.width() / 2, this.height() / 2];
      },

      getPlayer: function() {
        return player;
      },

      append: function(entity) {
        if (entity.html) this.html().append(entity.html());
        if (entity.name == 'player') player = entity;
        return this;
      },

      insert: function(entity) {
        Entities.insert(entity);
        return this;
      },

      remove: function(entity) {
        Entities.remove(entity);
        return this;
      },

      update: function(dt) {
        Entities.update(dt);

        var pop = Entities._population();
        $('.cell').removeClass('active');
        _.each(pop, function(val, index) {
          $('#cell-'+ index).addClass('active');  
        });
      },

      drawGrid: function() {
        var info = Entities._info();
        for (var i=0, len=info.cells.length; i<len; ++i) {
          var cell = $('<div>')
            .html(i)
            .addClass('cell')
            .attr('id', 'cell-'+ i)
            .css({
              width: info.cellsize[0], 
              height: info.cellsize[1],
              left: info.cells[i][0],
              top: info.cells[i][1]
            });

          this.html().append(cell);
        } 
      }
    };
  })();

  return Arena;

});
