define(['jquery', 'underscore', 'lib/inheritance', 'app/entity', 'app/bulletlist', 'app/bullet', 'app/arena', 'app/nexus'], function($, _, Class, Entity, BulletList, Bullet, Arena, Nexus) {

  var Player = Class.extend(Entity, {
    initialize: function() {
      this.parent('player');
      this.html().attr('id', 'player');

      this.bulletSpeed = 500;
      this.dbulletSpeed = Math.sqrt(Math.pow(this.bulletSpeed, 2) / 2);
      this.speed = 300;
      this.dspeed = Math.sqrt(Math.pow(this.speed, 2) / 2);

      this.bullet_rate = 100;
      this.bullet_timer = 0;

      this.moving = {
        up: false,
        down: false,
        left: false,
        right: false
      };

      this.shooting = {
        up: false,
        down: false,
        left: false,
        right: false
      };
    },

    offsets: function() {
      if (!this._offsets) {
        this._offsets = {
          centerx: (Player.size[0]/2) - (Bullet.size[0]/2),
          centery: (Player.size[1]/2) - (Bullet.size[1]/2),
          top: -(Bullet.size[1] + 2),
          bottom: Player.size[1] + 2,
          left: -(Bullet.size[0] + 2),
          right: Player.size[0] + 2 
        }
      }
      return this._offsets;
    },

    _shoot: function(direction, velocity) {
      BulletList.add(direction, velocity);
      this.bullet_timer = 0;
    },

    shootUp: function() {
      this._shoot([this.position[0] + this.offsets().centerx, this.position[1] + this.offsets().top], [0, -this.bulletSpeed]);
    },

    shootDown: function() {
      this._shoot([this.position[0] + this.offsets().centerx, this.position[1] + this.offsets().bottom], [0, this.bulletSpeed]);
    },

    shootLeft: function() {
      this._shoot([this.position[0] + this.offsets().left, this.position[1] + this.offsets().centery], [-this.bulletSpeed, 0]);
    },

    shootRight: function() {
      this._shoot([this.position[0] + this.offsets().right, this.position[1] + this.offsets().centery], [this.bulletSpeed, 0]);
    },

    shootUpLeft: function() {
      this._shoot([this.position[0] + this.offsets().left, this.position[1] + this.offsets().top], [-this.dbulletSpeed, -this.dbulletSpeed]);
    },

    shootUpRight: function() {
      this._shoot([this.position[0] + this.offsets().right, this.position[1] + this.offsets().top], [this.dbulletSpeed, -this.dbulletSpeed]);
    },

    shootDownLeft: function() {
      this._shoot([this.position[0] + this.offsets().left, this.position[1] + this.offsets().bottom], [-this.dbulletSpeed, this.dbulletSpeed]);
    },

    shootDownRight: function() {
      this._shoot([this.position[0] + this.offsets().right, this.position[1] + this.offsets().bottom], [this.dbulletSpeed, this.dbulletSpeed]);
    },

    collision: function(entity) {
      if (entity.name == 'enemy') {
        Nexus.damage();
        this.setPosition(Nexus.position.slice(0));
      }
    },

    update: function(dt) {
      this.bullet_timer += (dt * 1000);
      if (this.bullet_timer >= this.bullet_rate) {
        if (this.shooting.up && this.shooting.left) {
          this.shootUpLeft();
        } else if (this.shooting.up && this.shooting.right) {
          this.shootUpRight();
        } else if (this.shooting.down && this.shooting.left) {
          this.shootDownLeft();
        } else if (this.shooting.down && this.shooting.right) {
          this.shootDownRight();
        } else if (this.shooting.up) {
          this.shootUp();
        } else if (this.shooting.down) {
          this.shootDown();
        } else if (this.shooting.left) {
          this.shootLeft();
        } else if (this.shooting.right) {
          this.shootRight();
        }
      }
      
      this.velocity[0] = 0;
      this.velocity[1] = 0;

      if (this.moving.up && this.moving.left) {
        this.velocity[0] = -this.dspeed;
        this.velocity[1] = -this.dspeed;
      } else if (this.moving.up && this.moving.right) {
        this.velocity[0] =  this.dspeed;
        this.velocity[1] = -this.dspeed;
      } else if (this.moving.down && this.moving.left) {
        this.velocity[0] = -this.dspeed;
        this.velocity[1] =  this.dspeed;
      } else if (this.moving.down && this.moving.right) {
        this.velocity[0] =  this.dspeed;
        this.velocity[1] =  this.dspeed;
      } else if (this.moving.up) {
        this.velocity[1] = -this.speed;
      } else if (this.moving.down) {
        this.velocity[1] =  this.speed;
      } else if (this.moving.left) {
        this.velocity[0] = -this.speed;
      } else if (this.moving.right) {
        this.velocity[0] =  this.speed;
      }

      this.parent(dt);

      if (this.position[0] < 0) {
        this.position[0] = 0;
      } else if (this.position[0] + Player.size[0] > Arena.width()) {
        this.position[0] = Arena.width() - Player.size[0];
      }

      if (this.position[1] < 0) {
        this.position[1] = 0;
      } else if (this.position[1] + Player.size[1] > Arena.height()) {
        this.position[1] = Arena.height() - Player.size[1];
      }
    }
  });

  $(document).ready(function() {

    Player.size = (function(){
      var p = new Player();
      var el = p.html().appendTo('body').hide();
      var size = [el.width(), el.height()];
      el.remove();
      return size;
    })();

  });

  return Player;
});
