define(['jquery', 'underscore', 'lib/util', 'lib/vector', 'app/arena', 'app/enemylist'],
function($, _, Util, Vector, Arena, EnemyList) {

  var Emitter = function(waypoints, options) {
    this.waypoints = waypoints;
    this.position = waypoints[0].slice(0);

    this.nextWaypoint;
    this.direction;
    this.velocity;

    this.emit_timer = 0;

    this.settings = {
      speed: 50,
      type: 'circuit', // circuit  pace
      rate: 800,
      enemies: [
        { speed: 25 }
      ]
    }

    _.extend(this.settings, options);

    this.setWaypoint(1);
  };

  _.extend(Emitter.prototype, {

    remove: function() {
      if (this.$html) this.$html.remove();  
    },

    emit: function() {
      var enemy = this.settings.enemies[Util.range(0, this.settings.enemies.length-1)],
          direction = Vector.unit(Vector.subtract(Arena.center(), this.position)),
          velocity = Vector.scale(direction, enemy.speed);

      EnemyList.add(this.position.slice(0), velocity, enemy.target ? enemy.target : null); 

      this.emit_timer = 0;
    },

    gotoNextWaypoint: function() {
      if (this.nextWaypoint + 1 >= this.waypoints.length) {
        if ('pace' == this.settings.type) {
          this.waypoints.reverse();
          this.setWaypoint(1);
        } else {
          this.setWaypoint(0);
        }
      } else {
        this.setWaypoint(this.nextWaypoint + 1);
      }
    },

    targetWaypoint: function() {
      return this.waypoints[this.nextWaypoint];
    },

    setWaypoint: function(index) {
      this.nextWaypoint = index;
      this.direction = Vector.unit(Vector.subtract(this.waypoints[this.nextWaypoint], this.position));
      this.velocity = Vector.scale(this.direction, this.settings.speed); 
    },

    atTargetWaypoint: function() {
      return Vector.dot(this.direction, Vector.subtract(this.position, this.waypoints[this.nextWaypoint])) >= 0;
    },

    update: function(dt) {
      this.position[0] += this.velocity[0] * dt;
      this.position[1] += this.velocity[1] * dt;

      if (this.atTargetWaypoint()) {
        this.position = this.targetWaypoint().slice(0);
        this.gotoNextWaypoint();
      }

      if (this.$html) {
        this.$html.css({left:this.position[0], top:this.position[1]});
      }

      this.emit_timer += (dt * 1000);
      if (this.emit_timer >= this.settings.rate) {
        this.emit();
      }
    },

    render: function() {
      if (!this.$html) this.$html = $('<div>').addClass('emitter');
      this.$html.appendTo(Arena.html()); 
    }

  });

  return Emitter;

});
