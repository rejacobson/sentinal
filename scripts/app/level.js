define(['jquery', 'underscore', 'app/arena', 'app/enemylist', 'app/emitterlist', 'app/emitter', 'app/nexus', 'app/tokenmanager', 'app/dialog'],
function($, _, Arena, EnemyList, EmitterList, Emitter, Nexus, TokenManager, Dialog) {

  var P = {
    top: [375, -30],
    right: [750, 315],
    bottom: [375, 630],
    left: [-30, 315],

    topleft: [-30, -30],
    topright: [750, -30],
    bottomleft: [-30, 630],
    bottomright: [750, 630],

    top1_4: [187, -30],
    top3_4: [562, -30],

    right1_4: [750, 157],
    right3_4: [750, 472],

    bottom1_4: [187, 630],
    bottom3_4: [562, 630],

    right1_4: [-30, 157],
    right3_4: [-30, 472] 
  };

  var levels = {
    ////////////////////
    // 1
    ////////////////////
    1: function() {
      var emitter1 = new Emitter([P.bottomleft, P.topleft, P.topright, P.bottomright], {
        speed: 120,
        type: 'circuit',
        enemies: [
          { speed: 30 }
        ]
      });

      EmitterList.add(emitter1);

      TokenManager.next();
    },

    ////////////////////
    // 2
    ////////////////////
    2: function() {
      var emitter1 = new Emitter([P.bottomright, P.topright, P.topleft, P.bottomleft], {
        speed: 120,
        type: 'circuit',
        enemies: [
          { speed: 40, target: Arena.getPlayer().position }
        ]
      });

      EmitterList.add(emitter1);

      TokenManager.next();
    },

    ////////////////////
    // 3
    ////////////////////
    3: function() {
      var emitter1 = new Emitter([P.bottomleft, P.topleft, P.top], {
        speed: 120,
        type: 'pace',
        enemies: [
          { speed: 30 }
        ]
      });
      var emitter2 = new Emitter([P.bottomright, P.topright, P.top], {
        speed: 120,
        type: 'pace',
        enemies: [
          { speed: 50, target: Arena.getPlayer().position }
        ]
      });

      EmitterList.add(emitter1, emitter2);

      TokenManager.next();
    }
  } 

  var current_level = 0;
  
  var clearLevel = function() {
    TokenManager.reset();
    EmitterList.reset();  
    EnemyList.reset();
  }

  var loadLevel = function(num) {
    clearLevel();

    if (!levels[num]) {
      $(document).trigger('game.win');
    } else {
      $(document).trigger('game.wave', num);

      setTimeout(function(){
        Dialog.close();
        levels[num]();
      }, 2000);
    }
  }

  var Level = {
    next: function() {
      this.load(current_level + 1);
    },

    load: function(num) {
      current_level = num;
      loadLevel(num);
    }
  };

  return Level;

});
