define(['jquery', 'underscore', 'app/bullet', 'app/arena'], function($, _, Bullet, Arena) {

  var BulletList = (function(arena) {
    var bullets = [];
    var next = 0; 
    var max = 180;

    for (var i=0; i<max; ++i) {
      var bullet = new Bullet();
      Arena.append(bullet);
      bullet.retire();
      bullet.html().attr('id', 'bullet-'+ i);
      bullets.push(bullet);
    }

    var bullet, next_iterator, update_i;

    return {
      add: function(pos, vel) {
        if (next != null) {
          bullet = bullets[next];
          bullet.setPosition(pos).setVelocity(vel).activate(); 
        }
        this.getNext();
      },

      getNext: function() {
        if (next == null) next = 0;
        for (next_iterator=0; next_iterator<max; ++next_iterator) {
          next++;
          if (next >= max) next = 0;
          if (bullets[next].isRetired()) {
            break;
          }
        }
        if (next_iterator >= max) next = null; 
      },
    };
  })();

  return BulletList;

});
