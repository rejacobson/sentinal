define(['jquery', 'underscore', 'lib/inheritance', 'app/arena'], function($, _, Class, Arena) {

  var __id = 0;

  var Entity = Class.extend({
    initialize: function(name) {
      this.id = (__id++);
      this.name = name;
      this.position = [0, 0],
      this.last_position = [0, 0];
      this.velocity = [0, 0];
      this.active = true;
      this.interacting = true;
      this.$html = $('<div>').addClass('entity');

      this._style = this.$html[0].style;
    },

    isInteracting: function() { return this.interacting; },
    isActive:  function() { return this.active; },
    isRetired: function() { return !this.active; },

    activate: function() {
      this.active = true; 
      this.interacting = true;
      this.$html[0].style.display = '';
      Arena.insert(this);
      return this;
    },

    retire: function() {
      this.active = false;
      this.interacting = false;
      this.$html[0].style.display = 'none';
      Arena.remove(this);
      return this;
    },

    html: function() {
      return this.$html; 
    },

    size: function() {
      if (!this._size) this._size = [this.width(), this.height()];
      return this._size;
    },

    width: function() {
      if (!this._width) this._width = this.html().width();
      return this._width;
    },

    height: function() {
      if (!this._height) this._height = this.html().height();
      return this._height;
    },

    center: function() {
      return [this.position[0] + this.width() * 0.5, this.position[1] + this.height() * 0.5];
    },

    setVelocity: function(x, y) {
      if (_.isArray(x)) {
        y = x[1];
        x = x[0];
      }
      this.velocity = [x, y];
      return this;
    },

    setPosition: function(x, y) {
      if (_.isArray(x)) {
        y = x[1];
        x = x[0];
      }

      this.last_position[0] = this.position[0];
      this.last_position[1] = this.position[1];

      this.position[0] = x;
      this.position[1] = y;

      this._style.left = x +'px';
      this._style.top = y +'px';

      return this;
    },

    intersects: function(entity) {
      return this.position[0] < (entity.position[0] + entity.width()) &&
             this.position[0] + this.width() > entity.position[0] &&
             this.position[1] < (entity.position[1] + entity.height()) &&
             this.position[1] + this.height() > entity.position[1];
    },

    update: function(dt) {
      this.setPosition(this.position[0] + (this.velocity[0] * dt),
                       this.position[1] + (this.velocity[1] * dt));
    }
  });

  return Entity;

});
