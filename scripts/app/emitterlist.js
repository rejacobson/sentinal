define(['jquery', 'underscore', 'app/emitter', 'app/arena'], function($, _, Emitter, Arena) {

  var EmitterList = function() {
    this.emitters = [];

    this.add = function() {
      for (var i=0, len=arguments.length; i<len; ++i) {
        this.emitters.push(arguments[i]);
        //arguments[i].render();
      }
    }

    this.reset = function() {
      for (var i=0, len=this.emitters.length; i<len; ++i) {
        this.emitters[i].remove();
      }

      this.emitters = [];
    }

    this.update = function(dt) {
      for (var i=0, len=this.emitters.length; i<len; ++i) {
        this.emitters[i].update(dt);
      }
    }
  };

  return new EmitterList();

});
