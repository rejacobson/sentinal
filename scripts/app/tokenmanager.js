define(['jquery', 'underscore', 'lib/util', 'lib/inheritance', 'app/entity', 'app/arena', 'lib/vector'],
function($, _, Util, Class, Entity, Arena, Vector) {

  var Token = Class.extend(Entity, {
    initialize: function() {
      this.parent('token');
      this.html().attr('id', 'token');
    },

    activate: function() {
      this.html().show();
      this.parent();
      return this;
    },

    retire: function() {
      this.html().hide();
      this.parent();
      return this;
    },

    collision: function(entity) {
      if (entity.name == 'player') {
        this.retire();
        TokenManager.next();
      } else if (entity.name == 'enemy') {
        TokenManager.randomPosition();
      } else if (entity.name == 'nexus') {
        TokenManager.randomPosition();
      }
    } 
    
  });


  var TokenManager = (function() {
    var current = 0;
    var max_tokens = 10;
    var min_distance = Math.min(Arena.width(), Arena.height()) / 2 - 50;
    var min_distance_sq = Math.pow(min_distance, 2);
    var token = new Token();

    Arena.append(token);
    token.html().hide();

    return {
      next: function() {
        if (current >= max_tokens) {
          $(document).trigger('game.lasttoken');
          return;
        }

        current++;
        token.html().html(current);
        
        this.randomPosition();
      },

      randomPosition: function() {
        var w = Arena.width() - token.width() - 10,
            h = Arena.height() - token.height() - 10,
            new_x, new_y;

        do {
          new_x = Util.range(10, w);
          new_y = Util.range(10, h);
        } while (Vector.distanceSq([new_x, new_y], token.position) < min_distance_sq);

        token.setPosition([new_x, new_y]).activate(); 
      },
  
      reset: function() {
        current = 0;
      }
    };

  })();

  return TokenManager;

});
