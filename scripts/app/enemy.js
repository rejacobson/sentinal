define(['jquery', 'underscore', 'lib/inheritance', 'app/entity', 'lib/vector'], function($, _, Class, Entity, Vector) {

  var Enemy = Class.extend(Entity, {
    initialize: function() {
      this.parent('enemy');
      this.html().addClass('enemy');
      this.target;
      this.hp = 1;
      this.maxhp = 1;
    },

    setTarget: function(position) {
      this.target = position;
      if (this.target) {
        this.html().addClass('seeker');
      } else {
        this.html().removeClass('seeker');
      }
    },

    activate: function() {
      this.hp = this.maxhp;
      return this.parent();
    },

    collision: function(entity) {
      if (entity.name == 'bullet') {
        this.hp--;
        if (this.hp <= 0) {
          this.html().addClass('animate killed once half-second');
          this.velocity[0] = entity.velocity[0] / 2;
          this.velocity[1] = entity.velocity[1] / 2;
          this.interacting = false;
          var self = this;
          setTimeout(function(){
            self.html().removeClass('animate killed once half-second');
            self.retire();
          }, 1000);
        }
      } else if (entity.name == 'nexus') {
        this.retire();
      }
    },

    update: function(dt) {
      this.parent(dt);

      // Seek the target
      if (this.target && this.isInteracting()) {
        var angle1 = Vector.angle(this.target, this.position),
            angle2 = Vector.angle(this.velocity);
        Vector.rotate(this.velocity, angle2 - angle1);   
      }
    }
  });

  return Enemy;

});

