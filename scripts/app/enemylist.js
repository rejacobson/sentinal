define(['jquery', 'underscore', 'app/enemy', 'app/arena'], function($, _, Enemy, Arena) {

  var EnemyList = (function(arena) {
    var enemies = [];
    var next = 0; 
    var max = 100;

    for (var i=0; i<max; ++i) {
      var enemy = new Enemy();
      Arena.append(enemy);
      enemy.retire();
      enemy.html().attr('id', 'enemy-'+ i);
      enemies.push(enemy);
    }

    var enemy, next_iterator, update_i;

    return {
      add: function(pos, vel, target) {
        if (next != null) {
          enemy = enemies[next];
          enemy.setPosition(pos).setVelocity(vel).activate(); 
          if (target) {
            enemy.setTarget(target);
          } else {
            enemy.setTarget(null);
          }
        }
        this.getNext();
      },

      getNext: function() {
        if (next == null) next = 0;
        for (next_iterator=0; next_iterator<max; ++next_iterator) {
          next++;
          if (next >= max) next = 0;
          if (enemies[next].isRetired()) {
            break;
          }
        }
        if (next_iterator >= max) next = null; 
      },

      reset: function() {
        for (var i=0; i<max; ++i) {
          enemies[i].retire();
        }
      }
    };

  })();

  return EnemyList;

});

