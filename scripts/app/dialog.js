define(['jquery', 'lib/handlebars', 'text!/templates/dialog.html', 'text!/templates/instructions.html', 'text!/templates/win.html', 'text!/templates/gameover.html'],
function($, Handlebars, dialog, instructions, win, gameover) {

  var html = {
    dialog: Handlebars.compile(dialog),
    instructions: instructions,
    win: win,
    gameover: gameover
  };

  var Dialog = {
    create: function(title, message, buttons) {
      var content = html.dialog({title: title, message: message, buttons: buttons});
      $('#content').html(content);
      $('#dialog').fadeIn(1000);
    },

    open: function(name) {
      $('#content').html(html[name]);

      $('#content').find('a').click(function() {
        var $this = $(this), action = $this.data('action');
        $(document).trigger('game.'+ action);  
      });

      if (!$('#dialog').is(':visible')) {
        $('#dialog').fadeIn(1000);
      }
    },

    close: function() {
      $('#dialog').fadeOut(2000);
    }
  };

  return Dialog;

});
