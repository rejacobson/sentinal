define(['jquery', 'underscore', 'lib/inheritance', 'app/entity', 'app/arena', 'app/enemylist'], function($, _, Class, Entity, Arena, EnemyList) {

  var Nexus = Class.extend(Entity, {
    initialize: function() {
      this.parent('nexus');
      this.html().attr('id', 'nexus').addClass('animate rotate forever eight-seconds');
      this.html().append('<div class="core"></div><div class="core"></div><div class="core"></div><span id="primer"></span>');
      this.$primer = this.$html.find('#primer');
      this.health = 3;

      this.primer = 0;
      this.prime_limit = 50;
    },

    powerup: function() {
      this.primer++;
      this.$primer.html(this.primer);
      if (this.primer >= this.prime_limit) {
        this.html().addClass('primed');
      }
    },

    powerrelease: function() {
      EnemyList.reset();
      this.powerdown(); 
    },

    powerdown: function() {
      this.primer = 0;
      this.$primer.html('0');
      this.html().removeClass('primed');  
    },

    placeAtCenter: function() {
      var cx = Arena.html().width() / 2 - (this.width() / 2),
          cy = Arena.html().height() / 2 - (this.height() / 2);

      this.setPosition(cx, cy);
      return this;
    },

    restore: function() {
      this.health = 3;
      this.html().removeClass('health-0 health-1 health-2');
      this.activate();
    },

    damage: function() {
      if (this.health < 0) return;
      if (this.health == 0) {
        this.retire();
        $(document).trigger('game.gameover');
      }
      this.health--;
      this.powerdown();
      this.html().addClass('health-'+ this.health);  
    },

/*
    intersect: function(entity) {
      return Vector.lengthSq(Vector.subtract(entity.center(), this.center())) <= (this.width() * this.height() * 0.75);  
    },
*/

    collision: function(entity) {
      if (entity.name == 'enemy') {
        this.damage();
      } else if (entity.name == 'player' && this.primer >= this.prime_limit) {
        this.powerrelease(); 
      }
    },

    update: function(dt) {
      this.parent(dt);
    }
  });

  return new Nexus();

});

