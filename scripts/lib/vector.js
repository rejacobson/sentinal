define(function(){
  

  var toString = function(v) {
    if (!v) return 'undefined';
    return v[0] +':'+ v[1];
  };

  /**
   * @param {Array} origin point [a0, a1, a2]
   * @param {Array} target point [b0, b1, b2]
   * @returns {Number} distance between two points
   */
  var distance = function(a, b) {
     return len(subtract(a, b));
  };

  /**
   * @param {Array} origin point [a0, a1, a2]
   * @param {Array} target point [b0, b1, b2]
   * @returns {Number} distance^2 between two points
   */
  var distanceSq = function(a, b) {
     return lenSq(subtract(a, b));
  };

  /**
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} length of vector
   */
  var len = function(v) {
    return Math.sqrt(lenSq(v));
  };

  /**
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} length^2 of vector
   */
  var lenSq = function(v) {
    return v[0]*v[0] + v[1]*v[1];
  };

  /**
   * subtracts vectors [a0, a1, a2] - [b0, b1, b2]
   * @param {Array} a
   * @param {Array} b
   * @returns {Array} vector
   */
  var subtract = function(a, b) {
     return [a[0] - b[0], 
             a[1] - b[1]];
  };
  
  /**
   * adds vectors [a0, a1, a2] - [b0, b1, b2]
   * @param {Array} a vector
   * @param {Array} b vector
   * @returns {Array} vector
   */
  var add = function(a, b) {
     return [a[0] + b[0], 
             a[1] + b[1]];
  };
  
  /**
   * multiply a vector with another vector
   * @param {Array} vector [a0, a1]
   * @param {Array} vector
   * @returns {Array} result
   */
  var multiply = function(a, b) {
     return [a[0] * b[0],
             a[1] * b[1]];
  };
  
  /**
   * multiply a vector with a scalar
   * @param {Array} vector [a0, a1]
   * @param {Number} scale factor
   * @returns {Array} result
   */
  var scale = function(v, s) {
    return [v[0] * s, v[1] * s];
  };

  /**
   * @param {Array} a vector
   * @param {Number} s
   */
  var divide = function(a, s) {
     if (typeof s === 'number') {
      return [a[0] / s, 
              a[1] / s];
     }
     
     return [a[0] / s[0], 
             a[1] / s[1]];
  };
  
  
  /**
   *
   * normalize vector to unit vector
   * @param {Array} vector [v0, v1, v2]
   * @returns {Array} unit vector [v0, v1, v2]
   */
  var unit = function(v) {
     var l = len(v);
     if (l) return [v[0] / l, 
                    v[1] / l];
                   
     return [0, 0];
  };
  
  /**
   *
   * calculate vector dot product
   * @param {Array} vector [v0, v1, v2]
   * @param {Array} vector [v0, v1, v2]
   * @returns {Number} dot product of v1 and v2
   */
  var dot = function(a, b){
    return (a[0] * b[0]) + 
           (a[1] * b[1]);
  };

  /**
   * @returns {Array} vector with max length as specified.
   */
  var truncate = function(v, maxLength) {
    if (len(v) > maxLength) {
      return scale(unit(v), maxLength);
    };
    return v;
  };

  /**
   * Inline invert the direction of a vector
   */
  var invert = function(v) {
    v[0] = -v[0];
    v[1] = -v[1];
    return v;
  };

  
  /**
   * Return the inverse of a vector.
   */
  var inverse = function(v) {
    return [-v[0], -v[1]];
  }


  var normal = function(v) {
    return [-v[1], v[0]];
  };

  /**
   * rotate vector
   * @param {Array} vector [v0, v1]
   * @param {Number} angle to rotate vector by, radians. can be negative
   * @returns {Array} rotated vector [v0, v1]
   */
  var rotate = function(v, angle) {
    angle = normaliseRadians(angle);
    v[0] = v[0] * Math.cos(angle) - v[1] * Math.sin(angle);
    v[1] = v[0] * Math.sin(angle) + v[1] * Math.cos(angle);
    return v;
  };

  var rotation = function(v, angle) {
    return rotate(v.slice(0), angle); 
  };

  /**
   * calculate angle between vectors
   * @param {Array} vector [v0, v1]
   * @param {Array} vector [v0, v1]
   * @returns {Number} angle between v1 and v2 in radians
   */
  var angle = function(v1, v2) {
    var diff = v2 != undefined ? subtract(v1, v2) : v1;
    return Math.atan2(diff[0], diff[1]);
  };

  function normaliseRadians(radians) {
    radians = radians % (2*Math.PI);
    if (radians<0) {
      radians+=(2*Math.PI);
    }
    return radians;
  };
  
  return {
    toString: toString,
    distance: distance,
    distanceSq: distanceSq,
    len: len,
    lenSq: lenSq,
    subtract: subtract,
    add: add,
    multiply: multiply,
    scale: scale,
    divide: divide,
    unit: unit,
    dot: dot,
    truncate: truncate,
    invert: invert,
    inverse: inverse,
    normal: normal,
    rotate: rotate,
    rotation: rotation,
    angle: angle
  };
  
});
