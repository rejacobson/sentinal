define(['jquery', 'underscore'], function($, _){

  var Keycodes = {
    up: 38,
    down: 40,
    right: 39,
    left: 37,

    space: 32,
    backspace: 8,
    tab: 9,
    enter: 13,
    shift: 16,
    ctrl: 17,
    alt: 18,
    esc: 27,

    0: 48,
    1: 49,
    2: 50,
    3: 51,
    4: 52,
    5: 53,
    6: 54,
    7: 55,
    8: 56,
    9: 57,
    a: 65,
    b: 66,
    c: 67,
    d: 68,
    e: 69,
    f: 70,
    g: 71,
    h: 72,
    i: 73,
    j: 74,
    k: 75,
    l: 76,
    m: 77,
    n: 78,
    o: 79,
    p: 80,
    q: 81,
    r: 82,
    s: 83,
    t: 84,
    u: 85,
    v: 86,
    w: 87,
    x: 88,
    y: 89,
    z: 90,

    kp1: 97,
    kp2: 98,
    kp3: 99,
    kp4: 100,
    kp5: 101,
    kp6: 102,
    kp7: 103,
    kp8: 104,
    kp9: 105
  }

  var Input = (function() {
    var keypresses = {},
        callbacks = {
          keydown: {},
          keyup: {}
        };

    return {
      on: function(keypress, callback) {
        var parts = keypress.split('.'),
            type = parts[0],
            code = Keycodes[parts[1]];

        if (!callbacks[type][code]) callbacks[type][code] = [];

        callbacks[type][code].push(callback);

        return this;
      },

      keydown: function(key) {
        return keypresses[Keycodes[key]];
      },

      _onkeydown: function(event) {
        if (keypresses[event.keyCode]) return;
        keypresses[event.keyCode] = (new Date()).getTime();
        if (!callbacks.keydown[event.keyCode]) return;
        _.each(callbacks.keydown[event.keyCode], function(callback, index) {
          callback(event);
        });
      },

      _onkeyup: function(event) {
        keypresses[event.keyCode] = false;
        _.each(callbacks.keyup[event.keyCode], function(callback, index) {
          callback(event);
        });
      }
    };

  })();

  $(document).on('keydown', function(event) {
    Input._onkeydown(event);
  }).on('keyup', function(event) {
    Input._onkeyup(event);
  });

  return Input;

});
